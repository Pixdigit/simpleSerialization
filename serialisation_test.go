package simpleSerialization

import (
	"fmt"
	"strconv"
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

type testSerializable struct {
	TestData float64
}
func newTestSerializable(value float64) *testSerializable {
	return &testSerializable{value}
}
func (tS *testSerializable) TypeName() (string, error) {
	return "tS", nil
}
func (tS *testSerializable) String() (string, error) {
	return strconv.FormatFloat(tS.TestData, 'G', -1, 64), nil
}
func (tS testSerializable) FromString(s string) (Serializable, error) {
	value, err := strconv.ParseFloat(s, 64)
	return newTestSerializable(value), err
}

func TestSerialization(t *testing.T) {
	testVars := []interface{}{
		true, false, "true",
		3, 456, -234, -0,
		3.1415, -2.1645, -0.0,
		"4", "str:test", ":", "",
        newTestSerializable(0.5),
	}

    RegisterVariableType(newTestSerializable(2))

	for _, v := range testVars {
		enc, err := Serialize(v)
		if err != nil {
			tools.WrapErr(err, "error while serializing", t)
		} else {
			dec, err := Deserialize(enc)
			if err != nil {
				tools.WrapErr(err, "error while deserializing", t)
			} else {
                typeName, _ := TypeOfSerialized(enc)
                if typeName == "tS" {
                    tools.Test(dec.(*testSerializable).TestData == v.(*testSerializable).TestData, "var is not the same after de- and serializing: "+fmt.Sprint(v)+" != "+fmt.Sprint(dec), t)
                } else {
                    tools.Test(dec == v, "var is not the same after de- and serializing: "+fmt.Sprint(v)+" != "+fmt.Sprint(dec), t)
                }
			}
		}
	}

	testString := ""
	tools.TestAgainstStrings(
		func(str string) error {
			var err error
			testString, err = Serialize(str)
			return err
		},
		func() (string, error) {
			str, err := Deserialize(testString)
			return str.(string), err
		},
		"failure at serializing strings",
		t,
	)
}
