package simpleSerialization

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

type Serializable interface {
	String() (string, error)
	FromString(string) (Serializable, error)
	TypeName() (string, error)
}

var variableTypes map[string]Serializable

func RegisterVariableType(variable Serializable) error {
    //if variableTypes has not been initialized: do it now
    if len(variableTypes) == 0 {
        variableTypes = make(map[string]Serializable)
    }

	varTypeName, err := variable.TypeName();	if err != nil {return errors.Wrap(err, "could not read type of variable")}

	if strings.Contains(varTypeName, ":") {
		return errors.New("Variable type name can not contain a \":\"")
	}

	for typeName, _ := range variableTypes {
		if varTypeName == typeName {
			return errors.New("Variable name is already registered")
		}
	}

	variableTypes[varTypeName] = variable
	return nil
}

func Serialize(variable interface{}) (string, error) {
	var result string
	var err error

	switch t := variable.(type) {
	case bool:
		if t {
			result = "bool:true"
		} else {
			result = "bool:false"
		}
	case int64:
		result = "int:" + strconv.Itoa(int(t))
	case int32:
		result = "int:" + strconv.Itoa(int(t))
	case int:
		result = "int:" + strconv.Itoa(t)
	case float32:
		result = "float:" + strconv.FormatFloat(float64(t), 'G', -1, 64)
	case float64:
		result = "float:" + strconv.FormatFloat(t, 'G', -1, 64)
	case string:
		result = "str:" + t
	case Serializable:
		varType, err := t.TypeName();	if err != nil {return "", errors.Wrap(err, fmt.Sprintf("Can not get type name of %#v which is of type %T", t, t))}
		varValueString, err := t.String();	if err != nil {return "", errors.Wrap(err, fmt.Sprintf("Can not get type value of %#v which is of type %T", t, t))}
		result = varType + ":" + varValueString;	if err != nil {return "", errors.Wrap(err, fmt.Sprintf("Can not serialize %#v of type %T to string", t, t))}
	default:
		err = errors.New(fmt.Sprintf("Can not serialize %#v of type %T", t, t))
	}
	return result, err
}

func TypeOfSerialized(s string) (string, error) {
	if !strings.Contains(s, ":") {
		return "", errors.New("value is untyped")
	}
	return s[:strings.Index(s, ":")], nil
}

func Deserialize(raw string) (interface{}, error) {
	var result interface{}
	raw = strings.TrimLeft(raw, " ")
	varType, err := TypeOfSerialized(raw);	if err != nil {return nil, errors.Wrap(err, "could not deserialize \""+raw+"\"")}
	varValue := raw[strings.Index(raw, ":")+1:]

	switch varType {
	case "bool":
		if varValue == "true" {
			result = true
		} else if varValue == "false" {
			result = false
		} else {
			err = errors.New("could not deserialize " + varValue + " as boolean")
		}
	case "int":
		result, err = strconv.Atoi(varValue)
		errors.Wrap(err, "could not deserialize "+varValue+" as int")
	case "float":
		result, err = strconv.ParseFloat(varValue, 64)
		errors.Wrap(err, "could not deserialize "+varValue+" as float")
	case "str":
		result = varValue
	default:
        for typeName, deserializer := range variableTypes {
            if typeName == varType {
                return deserializer.FromString(varValue)
            }
        }

		err = errors.New("unknown var type in deserialisation " + varType)
	}
	return result, err
}
